tup1=('fruit', 'veg', 'non','78999990000')
print(max(tup1))
print(min(tup1))

tup2=(1,2,3)
tup3=tup1+tup2
print(tup3)

tup4=([1,2,3], ['a','b'])
print(tup4[0][0:2])
tup4[0][2]= 'updated' #list is updated not the tuple
print(tup4)

list1=list(tup4) #tuple converted to list
print(list1)
#can update and again convert back to tuple
tup5=tuple(list1)
print(tup5)
print(tup5 is tup4)
print(tup4)

for item in tup4:
    print(item) #returns list
    print(tuple(item)) #returns tuple