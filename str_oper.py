string='python'
print(len(string))
print(string[0:3])
print('t' in string)
print(string[2])

name= 'shubham'
age= 24
print('my name is %s and age is %d' % (name,age))
#will not concatenate int to string: print('my name is ' + name + 'and age is' + age)
print('my name is ' + name + 'and age is' + str(age))

print(name.capitalize()) #capitalizes only 1st letter
print(name.count("hub",1,len(name))) # 1 indicates from where to start the search. so 0 is fine not 2.

s=name.encode('utf-8', 'strict')
print(s)

print(name.replace('h', '-aa-',1))
print(name.replace('h', '-aa-',2))
print(name.upper()) #capitalizes evry letter
print(name.index('h',2)) #index of 2nd h
print(name.index('h',1)) #index od 1st h
print(name.index('h')) #index od 1st h ;returns error if not found
print(name.find('h',2)) #returns -1 if not found
print(name[::-1])
print(name)

print(max(name))