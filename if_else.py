x=10
y=12

if(x<y):
    print('x is less than y')
elif(x>y):
    print('x is greater than y')
else:
    print('x and y are equal')

#if else statement
a=30

if(a<10):
    print('less than ten')
if(10<=a<=25):
    print('in between 10 and 25')
else:
    print('greater than 25')

