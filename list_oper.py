list=['fruit', 'veg', 'non']
print(list[1:2])
print(list[-1])
print(list[-3])

print(list + ['one', 'two']) #concatenate
print(list*2)
print('veg' in list, 'non' not in list )

#update
list[1]='python'
print(list)

del(list[2])
print(list)

#print(list.pop(1))
#pop also remves the element
#print(list)
list1=[1,2,3,4]
print(list1.remove(1))
print(list1)

list2=[x**3 for x in list1]
print(list2)
#or
list3=[]
for x in list1:
    list3.append(x**3)
print(list3)

lis=[1,2]
lis.extend(['a','b']) #also concatenation   append works for adding 1 element
print(lis)

lis.insert(2,'aa')

lis1=[1,2,4,0]
#print(lis1.sort())
print(lis1)
print(sorted(lis1))
print(lis1)
print(lis1[::-1]) #reverse
print(lis1)

list4=[('a','b','c'), (1,2,3)]
print(list4[0][0:2])
print(len(list4[0]))